-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 12, 2021 at 05:30 AM
-- Server version: 10.4.20-MariaDB
-- PHP Version: 7.3.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pizza_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `combomenu`
--

CREATE TABLE `combomenu` (
  `id` bigint(20) NOT NULL,
  `duong_kinh` int(11) DEFAULT NULL,
  `kich_co` varchar(255) DEFAULT NULL,
  `salad` int(11) DEFAULT NULL,
  `so_luong_nuoc_uong` int(11) DEFAULT NULL,
  `suon` int(11) DEFAULT NULL,
  `thanh_tien` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `combomenu`
--

INSERT INTO `combomenu` (`id`, `duong_kinh`, `kich_co`, `salad`, `so_luong_nuoc_uong`, `suon`, `thanh_tien`) VALUES
(1, 20, 'S', 200, 2, 2, 150000),
(2, 25, 'M', 300, 3, 4, 200000),
(3, 30, 'L', 500, 4, 8, 250000);

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE `country` (
  `id` bigint(20) NOT NULL,
  `country_code` varchar(255) DEFAULT NULL,
  `country_name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`id`, `country_code`, `country_name`) VALUES
(1, 'VN101', 'Việt Nam'),
(2, 'JP102', 'Japan'),
(3, 'KR103', 'Korea'),
(4, 'CH104', 'China');

-- --------------------------------------------------------

--
-- Table structure for table `drinks`
--

CREATE TABLE `drinks` (
  `id` bigint(20) NOT NULL,
  `don_gia` bigint(20) DEFAULT NULL,
  `ghi_chu` varchar(255) DEFAULT NULL,
  `ma_nuoc_uong` varchar(255) DEFAULT NULL,
  `ngay_cap_nhat` datetime DEFAULT NULL,
  `ngay_tao` datetime DEFAULT NULL,
  `ten_nuoc_uong` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `drinks`
--

INSERT INTO `drinks` (`id`, `don_gia`, `ghi_chu`, `ma_nuoc_uong`, `ngay_cap_nhat`, `ngay_tao`, `ten_nuoc_uong`) VALUES
(2, 10000, NULL, 'TRATAC', '2021-08-10 10:22:30', '2021-08-12 10:22:30', 'Trà tắc'),
(3, 15000, NULL, 'COCA', '2021-08-10 10:22:46', '2021-08-12 10:22:46', 'Cocacola'),
(4, 15000, NULL, 'PEPSI', '2021-08-10 10:22:57', '2021-08-12 10:22:57', 'Pepsi'),
(5, 5000, NULL, 'LAVIE', '2021-08-10 10:23:07', '2021-08-12 10:23:07', 'Lavie'),
(6, 40000, NULL, 'TRASUA', '2021-08-10 10:23:17', '2021-08-12 10:23:17', 'Trà sữa trân châu'),
(7, 15000, NULL, 'FANTA', '2021-08-10 10:23:25', '2021-08-12 10:23:25', 'Fanta');

-- --------------------------------------------------------

--
-- Table structure for table `hibernate_sequence`
--

CREATE TABLE `hibernate_sequence` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `hibernate_sequence`
--

INSERT INTO `hibernate_sequence` (`next_val`) VALUES
(15);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` bigint(20) NOT NULL,
  `order_code` varchar(255) DEFAULT NULL,
  `paid` bigint(20) DEFAULT NULL,
  `pizza_size` varchar(255) DEFAULT NULL,
  `pizza_type` varchar(255) DEFAULT NULL,
  `price` bigint(20) DEFAULT NULL,
  `voucher_code` varchar(255) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `order_code`, `paid`, `pizza_size`, `pizza_type`, `price`, `voucher_code`, `user_id`) VALUES
(1, 'OPiz101', 180000, 'S', 'Hải sản', 200000, '12354', 1),
(2, 'OPiz102', 225000, 'M', 'Hawaii', 250000, '12365', 2),
(3, 'OPiz103', 225000, 'M', 'Thịt hun khói', 250000, '12378', 3),
(4, 'OPiz104', 270000, 'L', 'Hawaii', 300000, '12389', 4),
(5, 'OPiz105', 180000, 'S', 'Hải sản', 200000, '13256', 5),
(6, 'OPiz106', 225000, 'M', 'Thịt hun khói', 250000, '13654', 1);

-- --------------------------------------------------------

--
-- Table structure for table `region`
--

CREATE TABLE `region` (
  `id` bigint(20) NOT NULL,
  `region_code` varchar(255) DEFAULT NULL,
  `region_name` varchar(255) DEFAULT NULL,
  `country_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `region`
--

INSERT INTO `region` (`id`, `region_code`, `region_name`, `country_id`) VALUES
(1, 'HN', 'Hà Nội', 1),
(2, 'HCM', 'Hồ Chí Minh', 1),
(3, 'DN', 'Đà Nẵng', 1),
(4, 'TK', 'Tokyo', 2),
(5, 'OSA', 'Osaka', 2),
(6, 'KB', 'Kobe', 2),
(7, 'SE', 'Seoul', 3),
(8, 'BUS', 'Busan', 3),
(9, 'JE', 'Jeju', 3),
(10, 'NY', 'New York', 4),
(11, 'HA', 'Hawaii', 4),
(12, 'CHI', 'Chicago', 4),
(20, 'SP', 'Sapa', 1),
(25, 'HL', 'Hạ Long', 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) NOT NULL,
  `address` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `full_name` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `address`, `email`, `full_name`, `phone`) VALUES
(1, '55 Trần Đình Xu Quận 1 HCM', 'tam_thien@gmail.com', 'Thiên Tâm', '0912345678'),
(2, '23 Nguyễn Đình Chiểu Q1 HCM', 'ha_nguyen@gmail.com', 'Nguyễn Hà', '0122345678'),
(3, '34 Lương Nhữ Học Q5 HCM', 'toan_tran@gmail.com', 'Trần Toàn', '09093456789'),
(4, '111 Lý Thái Tổ Q10 HCM', 'phong_ngo@gmail.com', 'Ngô Phong', '0903987654'),
(5, '545 Nguyễn Huệ Đà Nẵng', 'anh_hoang@gmail.com', 'Hoàng Anh', '0907456789');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `combomenu`
--
ALTER TABLE `combomenu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UK_oqixmig4k8qxc8oba3fl4gqkr` (`country_code`);

--
-- Indexes for table `drinks`
--
ALTER TABLE `drinks`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UK_4ioautl9bhwm9nmdgersk8mjx` (`ma_nuoc_uong`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UK_dhk2umg8ijjkg4njg6891trit` (`order_code`),
  ADD UNIQUE KEY `UK_kdqb24llmcg5bi73wxp74bt5m` (`voucher_code`),
  ADD KEY `FK32ql8ubntj5uh44ph9659tiih` (`user_id`);

--
-- Indexes for table `region`
--
ALTER TABLE `region`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UK_is5udyhip6itt1wt8tj8hx5wq` (`region_code`),
  ADD KEY `lastfk_countryID` (`country_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `country`
--
ALTER TABLE `country`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `region`
--
ALTER TABLE `region`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `country`
--
ALTER TABLE `country`
  ADD CONSTRAINT `newfk_countryid` FOREIGN KEY (`id`) REFERENCES `region` (`country_id`) ON DELETE CASCADE;

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `FK32ql8ubntj5uh44ph9659tiih` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `region`
--
ALTER TABLE `region`
  ADD CONSTRAINT `lastfk_countryID` FOREIGN KEY (`country_id`) REFERENCES `country` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
