package com.pizza365.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pizza365.model.CDrink;

public interface IDrinkRepository extends JpaRepository<CDrink, Long> {

}
