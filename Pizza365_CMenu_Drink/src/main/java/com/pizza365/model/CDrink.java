package com.pizza365.model;

import java.util.Date;

import javax.persistence.*;
import javax.validation.constraints.*;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name = "drinks")
public class CDrink {
	public CDrink(String maNuocUong, String tenNuocUong, long donGia, String ghiChu, Date ngayTao, Date ngayCapNhat) {
		this.maNuocUong = maNuocUong;
		this.tenNuocUong = tenNuocUong;
		this.donGia = donGia;
		this.ghiChu = ghiChu;
		this.ngayTao = ngayTao;
		this.ngayCapNhat = ngayCapNhat;
	}

	public CDrink() {

	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	@NotNull(message = "Nhập mã nước uống")
	@Size(min = 4, message = "Mã nước uống phải có ít nhất 4 ký tự ")
	@Pattern(regexp = "(?=.*[A-Z])[\\p{Punct}A-Z0-9 ]{1,32}", message = "Mã nước uống phải viết IN HOA")
	@Column(name = "ma_nuoc_uong", unique = true)
	private String maNuocUong;

	@NotEmpty(message = "Nhập giá trị tên nước uống")
	@Column(name = "ten_nuoc_uong")
	private String tenNuocUong;
	
	@Min(value = 4999, message = "Đơn giá phải lớn hơn 5000")
	@Column(name = "don_gia")
	private long donGia;

	@Column(name = "ghi_chu")
	private String ghiChu;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "ngay_tao", nullable = true, updatable = false)
	@CreatedDate
	@JsonFormat(pattern = "dd-MM-yyyy")
	private Date ngayTao;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "ngay_cap_nhat", nullable = true)
	@LastModifiedDate
	@JsonFormat(pattern = "dd-MM-yyyy")
	private Date ngayCapNhat;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getMaNuocUong() {
		return maNuocUong;
	}

	public void setMaNuocUong(String maNuocUong) {
		this.maNuocUong = maNuocUong;
	}

	public String getTenNuocUong() {
		return tenNuocUong;
	}

	public void setTenNuocUong(String tenNuocUong) {
		this.tenNuocUong = tenNuocUong;
	}

	public long getDonGia() {
		return donGia;
	}

	public void setDonGia(long donGia) {
		this.donGia = donGia;
	}

	public String getGhiChu() {
		return ghiChu;
	}

	public void setGhiChu(String ghiChu) {
		this.ghiChu = ghiChu;
	}

	public Date getNgayTao() {
		return ngayTao;
	}

	public void setNgayTao(Date ngayTao) {
		this.ngayTao = ngayTao;
	}

	public Date getNgayCapNhat() {
		return ngayCapNhat;
	}

	public void setNgayCapNhat(Date ngayCapNhat) {
		this.ngayCapNhat = ngayCapNhat;
	}
}
