package com.pizza365.model;

import javax.persistence.*;

@Entity
@Table(name = "combomenu")
public class CMenu {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	@Column(name = "kich_co")
	private String kichCo;
	
	@Column(name = "duong_kinh")
	private int duongKinh;
	
	@Column(name = "suon")
	private int suon;
	
	@Column(name = "salad")
	private int salad;
	
	@Column(name = "so_luong_nuoc_uong")
	private int soLuongNuocUong;
	
	@Column(name = "thanh_tien")
	private long thanhTien;

	public CMenu() {

	}

	public CMenu(String kichCo, int duongKinh, int suon, int salad, int soLuongNuocUong, long thanhTien) {
		this.kichCo = kichCo;
		this.duongKinh = duongKinh;
		this.suon = suon;
		this.salad = salad;
		this.soLuongNuocUong = soLuongNuocUong;
		this.thanhTien = thanhTien;
	}

	/**
	 * @return the kichCo
	 */
	public String getKichCo() {
		return kichCo;
	}

	/**
	 * @param kichCo the kichCo to set
	 */
	public void setKichCo(String kichCo) {
		this.kichCo = kichCo;
	}

	/**
	 * @return the duongKinh
	 */
	public int getDuongKinh() {
		return duongKinh;
	}

	/**
	 * @param duongKinh the duongKinh to set
	 */
	public void setDuongKinh(int duongKinh) {
		this.duongKinh = duongKinh;
	}

	/**
	 * @return the suon
	 */
	public int getSuon() {
		return suon;
	}

	/**
	 * @param suon the suon to set
	 */
	public void setSuon(int suon) {
		this.suon = suon;
	}

	/**
	 * @return the salad
	 */
	public int getSalad() {
		return salad;
	}

	/**
	 * @param salad the salad to set
	 */
	public void setSalad(int salad) {
		this.salad = salad;
	}

	/**
	 * @return the soLuongNuocUong
	 */
	public int getSoLuongNuocUong() {
		return soLuongNuocUong;
	}

	/**
	 * @param soLuongNuocUong the soLuongNuocUong to set
	 */
	public void setSoLuongNuocUong(int soLuongNuocUong) {
		this.soLuongNuocUong = soLuongNuocUong;
	}

	/**
	 * @return the thanhTien
	 */
	public long getThanhTien() {
		return thanhTien;
	}

	/**
	 * @param thanhTien the thanhTien to set
	 */
	public void setThanhTien(long thanhTien) {
		this.thanhTien = thanhTien;
	}

	

}
