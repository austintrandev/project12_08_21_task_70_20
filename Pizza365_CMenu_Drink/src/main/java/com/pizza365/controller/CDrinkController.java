package com.pizza365.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.pizza365.model.*;
import com.pizza365.repository.*;

@RestController
@RequestMapping("/")
public class CDrinkController {
	@Autowired
	IDrinkRepository pDrinkRepository;

	@CrossOrigin
	@GetMapping("/drinks")
	public ResponseEntity<List<CDrink>> getAllDrinks() {
		try {
			List<CDrink> pDrinks = new ArrayList<CDrink>();

			pDrinkRepository.findAll().forEach(pDrinks::add);

			return new ResponseEntity<>(pDrinks, HttpStatus.OK);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@CrossOrigin
	@GetMapping("/drinks/{id}")
	public ResponseEntity<CDrink> getCDrinkById(@PathVariable("id") long id) {
		Optional<CDrink> drinkData = pDrinkRepository.findById(id);
		if (drinkData.isPresent()) {
			return new ResponseEntity<>(drinkData.get(), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		}
	}

	@CrossOrigin
	@PostMapping("/drinks")
	public ResponseEntity<CDrink> createCDrink(@Valid @RequestBody CDrink pDrinks) {
		try {
			pDrinks.setNgayTao(new Date());
			pDrinks.setNgayCapNhat(null);
			CDrink _drinks = pDrinkRepository.save(pDrinks);
			return new ResponseEntity<>(pDrinks, HttpStatus.CREATED);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@CrossOrigin
	@PutMapping("/drinks/{id}")
	public ResponseEntity<CDrink> updateCDrinkById(@PathVariable("id") long id, @RequestBody CDrink pDrinks) {
		Optional<CDrink> drinkData = pDrinkRepository.findById(id);
		if (drinkData.isPresent()) {
			CDrink drink = drinkData.get();
			drink.setMaNuocUong(pDrinks.getMaNuocUong());
			drink.setTenNuocUong(pDrinks.getTenNuocUong());
			drink.setDonGia(pDrinks.getDonGia());
			drink.setGhiChu(pDrinks.getGhiChu());
			drink.setNgayCapNhat(new Date());
			return new ResponseEntity<>(pDrinkRepository.save(drink), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@CrossOrigin
	@DeleteMapping("/drinks/{id}")
	public ResponseEntity<CDrink> deleteCDrinkById(@PathVariable("id") long id) {
		try {
			pDrinkRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
