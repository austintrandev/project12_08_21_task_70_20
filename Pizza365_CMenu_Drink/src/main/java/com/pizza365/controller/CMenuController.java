package com.pizza365.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.pizza365.model.*;
import com.pizza365.repository.*;

@RestController
public class CMenuController {
	@Autowired
	private IMenuRepository menuRepository;

	@CrossOrigin
	@GetMapping("/menu/all")
	public List<CMenu> getAllMenu() {
		return menuRepository.findAll();
	}

	@CrossOrigin
	@GetMapping("/menu/details/{id}")
	public CMenu getMenuById(@PathVariable Long id) {
		if (menuRepository.findById(id).isPresent())
			return menuRepository.findById(id).get();
		else
			return null;
	}

	@CrossOrigin
	@PostMapping("/menu/create")
	public ResponseEntity<Object> createMenu(@RequestBody CMenu cMenu) {
		try {
			CMenu newMenu = new CMenu();
			newMenu.setDuongKinh(cMenu.getDuongKinh());
			newMenu.setKichCo(cMenu.getKichCo());
			newMenu.setSalad(cMenu.getSalad());
			newMenu.setSoLuongNuocUong(cMenu.getSoLuongNuocUong());
			newMenu.setSuon(cMenu.getSuon());
			newMenu.setThanhTien(cMenu.getThanhTien());
			CMenu savedMenu = menuRepository.save(newMenu);
			return new ResponseEntity<>(savedMenu, HttpStatus.CREATED);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PutMapping("/menu/update/{id}")
	public ResponseEntity<Object> updateMenu(@PathVariable("id") Long id, @RequestBody CMenu cMenu) {
		Optional<CMenu> menuData = menuRepository.findById(id);
		if (menuData.isPresent()) {
			CMenu newMenu = menuData.get();
			newMenu.setDuongKinh(cMenu.getDuongKinh());
			newMenu.setKichCo(cMenu.getKichCo());
			newMenu.setSalad(cMenu.getSalad());
			newMenu.setSoLuongNuocUong(cMenu.getSoLuongNuocUong());
			newMenu.setSuon(cMenu.getSuon());
			newMenu.setThanhTien(cMenu.getThanhTien());
			CMenu savedMenu = menuRepository.save(newMenu);
			return new ResponseEntity<>(savedMenu, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	@CrossOrigin
	@DeleteMapping("/menu/delete/{id}")
	public ResponseEntity<Object> deleteMenuById(@PathVariable Long id) {
		try {
			Optional<CMenu> optional= menuRepository.findById(id);
			if (optional.isPresent()) {
				menuRepository.deleteById(id);
			}else {
			}			
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
